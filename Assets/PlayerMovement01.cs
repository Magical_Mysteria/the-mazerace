﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement01 : MonoBehaviour
{

    public CharacterController controller;
    public float speed = 12f;
    public float gravity = -9.81f;
    public float jumpHeight = 3f;
    public float DoubleJumpHeight = 4f;
    public float SpecialJumpHeight = 62;
    public Vector3 startPosition;
    private float JumpTimer = 20;
    public int MaxSecondsTillNextJump = 20;

    void Start()
    {
        startPosition = transform.position;
    }


    public void resetPlayer()
    {
        controller.enabled = false;
        transform.position = startPosition;
        controller.enabled = true;
        print(startPosition);
    }

    //Groundcheck
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    Vector3 velocity;
    public bool isGrounded;
    bool isInTheAir = false;

    // Update is called once per frame
    void Update()
    {

        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);


        //special jump
        if (Input.GetButtonDown("Special Jump") && isInTheAir && JumpTimer >= MaxSecondsTillNextJump)
        {
            JumpTimer = 0;
            velocity.y = Mathf.Sqrt(SpecialJumpHeight * -2f * gravity);
            isInTheAir = false;
        }

        //double jump
        if (Input.GetButtonDown("Jump") && isInTheAir)
        {

            velocity.y = Mathf.Sqrt(DoubleJumpHeight * -2f * gravity);
            isInTheAir = false;
        }
        // normal jump
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            isInTheAir = true;
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }


        // crouch
        if (Input.GetButton("Crouch"))
        {
            Crouch(true);
        }
        else
        {
            Crouch(false);
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.deltaTime);

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);

        print(JumpTimer);
        JumpTimer += Time.deltaTime;

    }


    public CapsuleCollider col;
    private void Crouch(bool isCrouching)
    {
        if (isCrouching == true)
        {
            transform.localScale = new Vector3(transform.localScale.x, 0.1f, transform.localScale.z);
            controller.radius = 0.5f;
            controller.height = 0.5f;
        }
        else
        {
            transform.localScale = new Vector3(transform.localScale.x, 1, transform.localScale.z);
            controller.radius = 1;
            controller.height = 1;
        }
    }


}
