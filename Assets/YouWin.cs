﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YouWin : MonoBehaviour
{
    [SerializeField] private Canvas winScreen;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            winScreen.enabled = true;
        }
    }
}
